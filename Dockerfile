FROM alpine:3.20
RUN apk add --no-cache appstream-generator logrotate jq python3

WORKDIR /cache/export
COPY asgen-config-generator.py /usr/local/bin/
COPY cron /etc/periodic
COPY logrotate/appstream-generator.conf /etc/logrotate.d/
# Logrotate requires 644 permissions, and it seems like the container
# built in CI might ignore the premissions set in this repository:
# https://gitlab.alpinelinux.org/alpine/infra/docker/gitlab-runner-helper/-/blob/master/scripts/gitlab-runner-build#L2
RUN chmod 644 /etc/logrotate.d/appstream-generator.conf
