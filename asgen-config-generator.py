#!/usr/bin/python3
#
# asgen-config-generator.py -- generate asgen-config file from template
#
# Copyright (C) 2022 Pablo Correa Gomez
#


from datetime import date
import json
import urllib.request


ALLOWLIST_ARCHES =  ["aarch64", "armhf", "armv7", "x86", "x86_64"]
TODAY = date.today().isoformat()

with open("/etc/appstream-generator/asgen-config.json.in", "r") as f:
    template = json.loads(f.read())

with urllib.request.urlopen("https://alpinelinux.org/releases.json") as url:
    releases = json.loads(url.read().decode())

suites = {}
for release in releases["release_branches"]:
    suite = {}
    sections = []
    architectures = []
    try:
        if release["eol_date"] < TODAY:
            continue
    except KeyError:
        pass

    for repo in release["repos"]:
        try:
            if repo["eol_date"] < TODAY:
                continue
        except KeyError:
            pass
        sections.append(repo["name"])
    architectures = [arch for arch in release["arches"] if arch in ALLOWLIST_ARCHES]
    suite["sections"] = sections
    suite["architectures"] = architectures
    suites[release["rel_branch"]] = suite

template["Suites"] = suites
with open("/etc/appstream-generator/asgen-config.json", "w") as f:
    f.write(json.dumps(template))
